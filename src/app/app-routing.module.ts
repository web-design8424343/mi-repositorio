//1er se crea el archivo app-routin.module.ts desde el cmd
//2do se reemplaza todo lo generado de fabrica por las lineas de codigo dadas por angular en su pagina tutorial
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeroesComponent } from './heroes/heroes.component';
//6to Abre AppRoutingModule e importa HeroDetailComponent.
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
//3er Importa el DashboardComponent en el AppRoutingModule.
import { DashboardComponent } from './dashboard/dashboard.component';

/*Una Ruta típica de Angular tiene dos propiedades:

path: una cadena que coincide con la URL en la barra de direcciones del navegador.
component: el componente que el enrutador debe crear al navegar a esta ruta.*/
const routes: Routes = [
  //se agrega la ruta de heroes
  { path: 'heroes', component: HeroesComponent },
  //4to Agrega una ruta al arreglo AppRoutingModule.routes que coincida con una ruta al DashboardComponent.
  { path: 'dashboard', component: DashboardComponent },
  //5to Para que la aplicación navegue al dashboard automáticamente, agrega la siguiente ruta al arreglo rutas.
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  //7mo agrega una ruta parametrizada al arreglo de rutas que coincida con el patrón de ruta de la vista detalle del héroe.
  { path: 'detail/:id', component: HeroDetailComponent },
  
];

@NgModule({
  //La siguiente línea agrega el RouterModule al arreglo AppRoutingModule de importartaciones y lo configura con las rutas en un solo paso llamando RouterModule.forRoot():
  imports: [ RouterModule.forRoot(routes) ],
  //A continuación, AppRoutingModule exporta el RouterModule para que esté disponible en toda la aplicación.
  exports: [ RouterModule ]
})
export class AppRoutingModule { }