import { Component } from '@angular/core';
//1er se importa el MessageService de message.service
import { MessageService } from '../message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent {
  //2do se modifica el constructor con un parámetro que declare una propiedad messageService publica. Angular inyectará el único MessageService en esa propiedad cuando crea el MessagesComponent.
  constructor(public messageService: MessageService) {}
  ngOnInit() {
  }
}
