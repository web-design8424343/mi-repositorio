//1er se importa Hero del archivo hero
import { Hero } from '../hero';
import { Component, OnInit, Input } from '@angular/core';
//3er Agrega las siguientes importaciones:
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { HeroService } from '../hero.service';


@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  //2do se agrega la propiedad hero antepuesta por el decorador input
  @Input()
  hero!: Hero;

  constructor(
    //4to se inyectan los servicios ActivatedRoute, HeroService y Location en el constructor, guardando sus valores en campos privados:
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) {}
    //5to ngOnInit es un metodo de ciclo de vida que llama a getHero
  ngOnInit(): void {
    this.getHero();
  }
  //6to y definelo de la siguiente manera
  getHero(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.heroService.getHero(id)
      .subscribe(hero => this.hero = hero);
  }
  //7mo Agrega un método goBack() a la clase de componente que navega hacia atrás un paso en la pila de historial del navegador usando el servicio Location que inyectaste previamente.
  goBack(): void {
    this.location.back();
  }
}
