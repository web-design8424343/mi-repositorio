//1er se importan los componenentes, objetos o material que se vaya a utilizar, despues se debe declarar en el "export class HeroesComponent"
import { Component } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
//en esta clase se ponen todos los valores importados
export class HeroesComponent {
[x: string]: any;
  //8vo se elimina el selectedhero y el onselec porque ya no se utilizan
  //2do se reemplaza al valor heroes con una declaracion
  heroes!: Hero[];
  //3ro Agregue un parámetro privado heroService de tipo HeroService al constructor / 
  constructor(private heroService: HeroService, private messageService: MessageService) { }
  //4to Crear metodo para recuperar a los heroes del servicio
  getHeroes(): void {
    //6to tiene firma sincronica, lo que implica que el HeroService puede buscar heroes sincronicamente. consume el resultado de getHeroes como si los heroes pudieran ser recuperados sincronicamente
    this.heroService.getHeroes()
      //7mo ahora devuelve un observable <Hero[]>
      .subscribe(heroes => this.heroes = heroes);
  }
  ngOnInit() {
    //5to se llama getHeroes en ngOnInit
    this.getHeroes();
  }
  
}
