import { Injectable } from '@angular/core';
//1ro importar Hero y HEROES
import { Hero } from './hero';
import { HEROES } from './mock-heroes';
//3ro importa simbolos observables y of de RxJS
import { Observable, of } from 'rxjs';
//4toimport { MessageService } from './message.service';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class HeroService {
  //5to se modifica el constructor con un parametro que declare una propiedad privada messageService. Angular inyectará el singleton MessageService en esa propiedad cuando crea el HeroService.
  constructor(private messageService: MessageService) { }
  //2do se agrega el metodo getHeroes y se devuelve los heroes simulados / 10mo se reemplaza el metodo getHeroes con los siguiente
  getHeroes(): Observable<Hero[]> {
    //6to Modifique el método getHeroes() para enviar un mensaje cuando se busquen los héroes.
    const heroes = of(HEROES);
    // TODO: manda el mensaje despues de _after_ fetching los
    this.messageService.add('HeroService: fetched heroes');
    return of(HEROES);
  }
  getHero(id: Number): Observable<Hero> {
    // TODO: send the message _after_ fetching the hero
    const hero = HEROES.find(h => h.id === id)!;
    this.messageService.add(`HeroService: fetsched hero id=${id}`);
    return of (hero);
  }
}
