import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor() { }
  //1er el servicio expone su caché de mensajes y dos métodos
  messages: string[] = [];
  //2do este es para agregar() un mensaje al caché
  add(message: string) {
    this.messages.push(message);
  }
  //3ro este es para borrar() el caché
  clear() {
    this.messages = [];
  }
}
