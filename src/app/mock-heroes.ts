//se crea la lista de heroes

import { Hero } from './hero';

export const HEROES: Hero[] = [
    { id: 11, name: 'Robin', poder: 'Ser hijo de Batman', pelicula: 'Batman Forever' },
    { id: 12, name: 'Superman', poder: 'Super fuerza, capacidad de volar, vision de rayos x, etc.', pelicula: 'Superman II' },
    { id: 13, name: 'Batman', poder: 'Dinero... mucho dinero', pelicula: 'The dark knight' },
    { id: 14, name: 'Wonder Woman', poder: 'Fuerza sobre humana y durabilidad', pelicula: 'Wonder Woman' },
    { id: 15, name: 'Flash', poder: 'Super velocidad', pelicula: 'Justice League' },
    { id: 16, name: 'Green Lantern', poder: 'Anillo de poder que puede crear formas solidas y formas de energia', pelicula: 'Green Lantern' },
    { id: 17, name: 'Aquaman', poder: 'Comunicarse con las criaturas marinas', pelicula: 'Aquaman' },
    { id: 18, name: 'Hawkgirl', poder: 'Volar', pelicula: 'Legends of Tomorrow' },
    { id: 19, name: 'Cyborg', poder: 'Cuerpo cibernetico', pelicula: 'Justice League' },
    { id: 20, name: 'Plastic Man', poder: 'Habilidades de estiramiento y flexibilidad', pelicula: 'Ninguna pelicula aun' }
  ];